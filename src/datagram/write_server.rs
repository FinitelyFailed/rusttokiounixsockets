use tokio::net::UnixDatagram;
use tokio::task::JoinHandle;
use tokio::sync::Mutex;
use tokio::net::unix::SocketAddr;
use std::sync::Arc;

pub struct WriteServer {
    socket_path: String,
    socket: Arc<Mutex<UnixDatagram>>,
    subscription_listener_task: JoinHandle<()>,
    current_data: Arc<Mutex<Vec<u8>>>,
    subscriptions: Arc<Mutex<Vec<String>>>,
}

impl WriteServer {
    pub fn new(socket_path: &str) -> anyhow::Result<WriteServer> {
        let socket = Arc::new(Mutex::new(super::create_socket(socket_path)?));

        let current_data = Arc::new(Mutex::new(vec!()));
        let subscriptions = Arc::new(Mutex::new(vec!()));

        let subscription_socket_path = format!("{}{}", socket_path, super::SUBSCRIPTION_POST_SOCKET_PATH);
        let subscription_listener_task = Self::start_subscription_listener(&subscription_socket_path, 
            subscriptions.clone(),
            current_data.clone(),
            socket.clone())?;

        Ok(WriteServer {
            socket_path: socket_path.to_string(),
            socket,
            subscription_listener_task,
            current_data,
            subscriptions,
        })
    }

    pub async fn write(&self, data: &str) {

        let bytes = data.as_bytes().to_vec();

        Self::send_to_all(self.subscriptions.clone(), self.socket.clone(), &bytes).await;

        {
            let mut data = self.current_data.lock().await;
            *data = bytes;
        }
    }

    async fn send_to_all(subscriptions: Arc<Mutex<Vec<String>>>, 
        socket: Arc<Mutex<UnixDatagram>>,
        data: &Vec<u8>) {

        let mut subs_to_be_removed = vec!();

        let subs_to_send = {
            let subs = subscriptions.lock().await;
            subs.clone()
        };

        {
            for sub in subs_to_send {
                let s = socket.lock().await;
                match s.send_to(data, sub.clone()).await {
                    Ok(_) => {}
                    Err(_e) => {
                        subs_to_be_removed.push(sub.clone());
                    }
                }
            }
        }

        {
            for sub_to_remove in subs_to_be_removed {
                let mut subs = subscriptions.lock().await;
                if let Some(index) = subs.iter().position(|x| x.to_string() == sub_to_remove) {
                    subs.remove(index);
                }
            }
        }
    }

    fn start_subscription_listener(subscription_socket_path: &str,
        subscriptions: Arc<Mutex<Vec<String>>>,
        current_data: Arc<Mutex<Vec<u8>>>,
        send_socket: Arc<Mutex<UnixDatagram>>) -> anyhow::Result<JoinHandle<()>> {

        let socket = super::create_socket(subscription_socket_path)?;
        let subscription_socket_path = subscription_socket_path.to_string();
        Ok(tokio::spawn(async move {
            loop {
                let mut buf = vec![0u8; 1];
                match socket.recv_from(&mut buf).await {
                    Ok((_, addr)) => {
                       Self::add_subscription(addr, 
                            subscriptions.clone(), 
                            current_data.clone(), 
                            send_socket.clone()).await;
                    }
                    Err(e) => println!("Failed to read from socket {subscription_socket_path} error: {e:?}"),
                }
            }
        }))
    }

    async fn add_subscription(addr: SocketAddr, 
        subscriptions: Arc<Mutex<Vec<String>>>,
        current_data: Arc<Mutex<Vec<u8>>>,
        socket: Arc<Mutex<UnixDatagram>>) {

        if let Some(path) = addr.as_pathname() {
            if let Some(sub_addr) = path.to_str() {
                let send_data = {
                    let mut subs = subscriptions.lock().await;
                    if subs.iter().position(|x| x.to_string() == sub_addr).is_some() {
                        false
                    } else {
                        subs.push(sub_addr.to_string());
                        println!("Adding new subscription {sub_addr}");
                        true
                    }
                };

                if send_data {
                    Self::send_data(sub_addr, socket, current_data).await;
                }
            }
        }
    }

    async fn send_data(addr: &str, 
        socket: Arc<Mutex<UnixDatagram>>, 
        current_data: Arc<Mutex<Vec<u8>>>) {
    
        let data = {
            current_data.lock().await.clone()
        };
       
        if !data.is_empty() {
            let socket = socket.lock().await;
            match socket.send_to(&data, addr).await {
                Ok(n) => println!("Wrote {n} bytes to {addr}"),
                Err(e) => println!("Failed to write to {addr}: {e:?}"),
            }
        }
    }
}

impl Drop for WriteServer {
    fn drop(&mut self) {
        self.subscription_listener_task.abort();
    }
}
