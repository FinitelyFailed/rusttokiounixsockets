use tokio::net::UnixDatagram;
use tokio::sync::broadcast;
use tokio::sync::broadcast::{
    Receiver,
    Sender,
};
use tokio::task::JoinHandle;
use tokio::time::{
    sleep,
    Duration,
};

pub struct ReadClient {
    // socket_path: String,
    sender: Sender<String>,
    read_task_handle: JoinHandle<()>,
}

impl ReadClient {
    pub fn new(socket_path: &str) -> anyhow::Result<ReadClient> {
        let socket = super::create_socket(socket_path)?;

        let (sender, _receiver) = broadcast::channel(1);
        let read_task_handle = Self::start_read_task(socket_path, socket, sender.clone());

        Ok(ReadClient {
            sender,
            read_task_handle,
        })
    }

    pub fn new_with_handshake(socket_path: &str, socket_path_writer: &str) -> anyhow::Result<ReadClient> {
        let socket = super::create_socket(socket_path)?;

        let (sender, _receiver) = broadcast::channel(1);
        let read_task_handle = Self::start_read_task_with_handshake(socket_path, socket_path_writer, socket, sender.clone());

        Ok(ReadClient {
            sender,
            read_task_handle,
        })
    }

    pub fn read_receiver(&self) -> Receiver<String> {
        self.sender.subscribe()
    }
    
    fn start_read_task(socket_path: &str, 
        socket: UnixDatagram, 
        sender: Sender<String>) -> JoinHandle<()> {

        let socket_path = socket_path.to_string();
        tokio::spawn(async move {
            let mut buf = [0u8; 4096];
            loop {
                Self::run_reader(&socket_path, &socket, &sender, &mut buf).await;
            }
        })
    }

    async fn run_reader(socket_path: &str, 
        socket: &UnixDatagram,
        sender: &Sender<String>,
        buf: &mut [u8]) {

        match &socket.recv_from(buf).await {
            Ok((size, addr)) => {
                let a = addr.as_pathname().unwrap();
                println!("Received {size} bytes from {a:?}");
                match std::str::from_utf8(&buf[..*size]) {
                    Ok(data) => {
                        match sender.send(data.to_string()) {
                            Ok(_) => println!("Successfully sent to receiver, data: {data}"),
                            Err(e) => println!("Failed to send data to receiver: {e:?}"),
                        }
                    }
                    Err(e) => println!("Failed to transform byte data to string: {e:?}"),
                }
            }
            Err(e) => println!("Failed to read from socket {socket_path} error: {e:?}"),
        }
    }

    fn start_read_task_with_handshake(socket_path: &str,
        socket_path_writer: &str,
        socket: UnixDatagram,
        sender: Sender<String>) -> JoinHandle<()> {

        let timeout = 1000;
        let socket_path = socket_path.to_string();
        let socket_path_writer = format!("{}{}", socket_path_writer, super::SUBSCRIPTION_POST_SOCKET_PATH);
        tokio::spawn(async move {
            loop {
                match socket.send_to(super::SUBSCRIPTION_HANDSHAKE, &socket_path_writer).await {
                    Ok(_) => {
                        println!("Handshake Successfully sent from {socket_path} to {socket_path_writer}");
                        let mut buf = [0u8; 4096];
                        loop {
                            if let Err(_) = tokio::time::timeout(Duration::from_millis(timeout), 
                            Self::run_reader(&socket_path, &socket, &sender, &mut buf)).await {
                                println!("No data from writer, for {timeout} ms");
                            }
                        }
                    }
                    Err(e) => {
                        println!("Handshake failed to be sent from {socket_path} to {socket_path_writer}, will retry in {timeout} ms, error: {e:?}");
                        sleep(Duration::from_millis(timeout)).await;
                    }
                }
            }
        })
    }
}

impl Drop for ReadClient {
    fn drop(&mut self) {
        self.read_task_handle.abort();
    }
}
