use tokio::net::UnixDatagram;

pub struct WriteClient {
    socket_path: String,
    socket: UnixDatagram,
}

impl WriteClient {
    pub fn new(socket_path: &str) -> anyhow::Result<WriteClient> {
        let socket = super::create_socket(socket_path)?;

        Ok(WriteClient {
            socket_path: socket_path.to_string(),
            socket, 
        })
    }

    pub async fn write(&self, data: &str, target_socket: &str) -> anyhow::Result<()> {
        let bytes = data.as_bytes();
        match self.socket.send_to(bytes, target_socket).await {
            Ok(n) => {
                let socket_path = &self.socket_path;
                println!("Wrote {n} bytes from {socket_path} to {target_socket}, data: {data}");
                Ok(())
            },
            Err(e) => Err(anyhow::Error::new(e)),
        }
    }
}
