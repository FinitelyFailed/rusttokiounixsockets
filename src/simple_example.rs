use std::str;
use tokio;
use tokio::sync::mpsc;
use tokio::sync::mpsc::Sender;
use tokio::time::{
    Duration,
    sleep,
};
use tokio::net::{
    UnixListener, 
    UnixStream,
};

const SOCKET_PATH: &str = "test_socket";
const DATA_GENERATION_TICK_MS: u64 = 1000;

// TODO: This function should open a connection to the socket and write a string, when done it is
// not needed to be connected any more.
fn start_unix_socket_client(socket_path: String) {
    tokio::spawn(async move {
        loop {
            match UnixStream::connect(socket_path.clone()).await {
                Ok(stream) => {
                    println!("Client connect to socket: {socket_path}");
                    let mut x = 0;
                    loop {
                        x = 1 + x;
                        let s = x.to_string();
                        let bytes = s.as_bytes();

                        match stream.writable().await {
                            Ok(_) => {
                                println!("Socket is writable will write.");
                                match stream.try_write(bytes) {
                                    Ok(n) => {
                                        println!("Written {n} bytes to stream, {s}");
                                    }
                                    Err(e) => {
                                        println!("Failed to write to stream, {e:?}");
                                    }
                                }
                            }
                            Err(e) => {
                                println!("Failed to wait for socket to be writeable: {e:?}");
                            }
                        }
                        sleep(Duration::from_millis(DATA_GENERATION_TICK_MS)).await;
                    }
                }
                Err(e) => {
                    println!("Failed to connect to socket {e:?}");
                }
            }
            sleep(Duration::from_millis(DATA_GENERATION_TICK_MS)).await;
        }
    });
}

fn start_unix_socket_server(socket_path: String, tx: Sender<String>) {
    tokio::spawn(async move {
        match tokio::fs::remove_file(socket_path.clone()).await {
            Ok(_) => {
                println!("Removed old socket: {socket_path}");
            }
            Err(e) => {
                println!("Failed to removed file: {e:?}");
            }
        }
        let listener_result = UnixListener::bind(socket_path);

        match listener_result {
            Ok(listener) => {
                println!("Server connected to socket");
                loop {
                    match listener.accept().await {
                        Ok((stream, _)) => {
                            loop {
                                match stream.readable().await {
                                    Ok(_) => {
                                        println!("Stream is readable will try to read");

                                        let mut buf = [0; 4096];
                                        // TODO: Handle when whole buffer is read.
                                        match stream.try_read(&mut buf) {
                                            Ok(0) => {
                                                println!("Empty read");
                                                tx.send("lol".to_string()).await;
                                            }
                                            Ok(n) => {
                                                // TODO: Append to data to string.
                                                // If n == 4096 number of bytes:
                                                // Reset buffer.
                                                // Try to read again.
                                                println!("Read {n} bytes");
                                                match str::from_utf8(&buf) {
                                                    Ok(s) => {

                                                        tx.send(s.to_string()).await;
                                                    }
                                                    Err(e) => {
                                                        println!("Failed to convert data to utf8 string, {e:?}");
                                                    }
                                                }
                                            }
                                            Err(e) => {
                                                // It is fine if this fails with would block.
                                                // Should not print, just handle.
                                                println!("Failed to read from stream, {e:?}");
                                            }
                                        }
                                    }
                                    Err(e) => {
                                        println!("Stream is not readable, will not try to read, {e:?}.");
                                    }
                                }
                            }
                        },
                        Err(e) => {
                            println!("Failed to something ... {e:?}");
                        }
                    }
                } 
            } Err(e) => {
                println!("Faild to create unix listener: {e:?}");
            }
        }
    });
}

async fn run() -> Result<(), Box<dyn std::error::Error>> {
    let (tx0, mut rx0) = mpsc::channel(32);

    start_unix_socket_server(SOCKET_PATH.to_string(), tx0);
    start_unix_socket_client(SOCKET_PATH.to_string());

    loop {
        match rx0.recv().await {
            Some(data) => {
                println!("Handle data: {data}");
            }
            None => {

            }
        }
    }
}
