use std::fs;
use tokio::net::UnixDatagram;

pub mod write_client;
pub mod write_server;
pub mod read_client;

const SUBSCRIPTION_POST_SOCKET_PATH: &str = "_subscription";
const SUBSCRIPTION_HANDSHAKE: &[u8] = &[42];

pub fn create_socket(socket_path: &str) -> anyhow::Result<UnixDatagram> {
    let socket_path = socket_path.to_string();
    match fs::remove_file(&socket_path) {
        Ok(_) => println!("Successfully removed file: {socket_path}"),
        Err(e) => println!("Failed to remove file: {e:?}"),
    }

    Ok(UnixDatagram::bind(socket_path)?)
}
