mod datagram;

use tokio;
use tokio::time::{
    sleep,
    Duration,
};
use datagram::{
    write_client::WriteClient,
    read_client::ReadClient,
    write_server::WriteServer,
};

#[tokio::main]
async fn main() -> Result<(), Box<dyn std::error::Error>> {
    // test_write_and_read_clients().await?;
    test_write_server_and_listen_clients().await?;

    Ok(())
}

async fn test_write_and_read_clients() -> anyhow::Result<()> {
    let number_of_messages = 10;
    let write_socket_path = "write_test_socket";
    let read_socket_path = "read_test_socket";

    let write_client = WriteClient::new(write_socket_path)?;
    let write_client_task = tokio::spawn(async move {

        let mut i = 0;
        loop {
            if i >= number_of_messages {
                break;
            }

            let message = format!("WriteClient: {}", i);
            match write_client.write(&message, &read_socket_path).await {
                Ok(_) => {
                    println!("Successfully wrote to socket: {read_socket_path}");
                    i = i + 1;
                    sleep(Duration::from_secs(5)).await;
                }
                Err(e) => {
                    println!("Failed to write to socket: {read_socket_path}: {e:?}");
                    sleep(Duration::from_millis(500)).await;
                }
            }
        }
    });

    let read_client = ReadClient::new(read_socket_path)?;
    let mut read_client_receiver = read_client.read_receiver();
    let read_client_task = tokio::spawn(async move {
        let mut i = 0;
        loop {
           match read_client_receiver.recv().await {
                Ok(data) => {
                    i = i + 1;
                    println!("Got message: {data:?}, {i} of {number_of_messages} received");
                }
                Err(e) => println!("Failed to receive message: {e:?}"),
            }

            if i >= number_of_messages {
                println!("Received number of expected messages, will exit.");
                break;
            }
        } 
    });

    write_client_task.await?;
    read_client_task.await?;

    Ok(())
}

async fn test_write_server_and_listen_clients() -> anyhow::Result<()> {
    let number_of_messages = 10;
    let write_socket_path = "write_test_socket";
    let read_socket_path_0 = "read_test_socket_0";
    let read_socket_path_1 = "read_test_socket_1";

    let write_server = WriteServer::new(write_socket_path)?;
    let write_server_task = tokio::spawn(async move {

        let mut i = 0;
        loop {
            if i >= number_of_messages {
                break;
            }

            let message = format!("WriteServer: {}", i);
            write_server.write(&message).await;
            i = i + 1;
            sleep(Duration::from_secs(2)).await;
        }
    });
    
    let read_client_0 = ReadClient::new_with_handshake(read_socket_path_0, write_socket_path)?;
    let mut read_client_receiver_0 = read_client_0.read_receiver();
    let read_client_task_0 = tokio::spawn(async move {
        let mut i = 0;
        loop {
            match read_client_receiver_0.recv().await {
                Ok(data) => {
                    i = i + 1;
                    println!("Got message: {data:?}, {i} of {number_of_messages} received");

                    if i >= number_of_messages {
                        println!("Received number of expected messages, will exit.");
                        break;
                    }

                }
                Err(e) => println!("Failed to receive message: {e:?}"),
            }

        } 
    });

    let read_client_1 = ReadClient::new_with_handshake(read_socket_path_1, write_socket_path)?;
    let mut read_client_receiver = read_client_1.read_receiver();
    let read_client_task_1 = tokio::spawn(async move {
        let mut i = 0;
        loop {
            match read_client_receiver.recv().await {
                Ok(data) => {
                    i = i + 1;
                    println!("Got message: {data:?}, {i} of {number_of_messages} received");

                    if i >= number_of_messages {
                        println!("Received number of expected messages, will exit.");
                        break;
                    }

                }
                Err(e) => println!("Failed to receive message: {e:?}"),
            }

        } 
    });

    write_server_task.await?;
    read_client_task_0.await?;
    read_client_task_1.await?;

    Ok(())
}
